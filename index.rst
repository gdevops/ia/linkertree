

|FluxWeb| `RSS <https://gdevops.frama.io/ia/linkertree/rss.xml>`_

.. _llms:

=============================================================
**Liens IA/LLMs**
=============================================================

- https://rstockm.github.io/mastowall/?hashtags=openllm&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=ia&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=llms&server=https://framapiaf.org

- https://gdevops.frama.io/ia/openllm-france
- https://gdevops.frama.io/ia/llms/
- https://til.simonwillison.net/
